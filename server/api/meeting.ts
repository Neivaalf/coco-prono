import { chromium } from 'playwright';

export default defineEventHandler(async(event) => {

    const browser = await chromium.launch();
    const context = await browser.newContext();
    const page = await context.newPage();

    await page.goto("https://www.formula1.com/en/racing/2024.html");

    const meetings = await page.locator('.race-card').all();

    return Promise.all(meetings.map(async meeting => {
        return {
            place: (await meeting.locator('.event-place').innerText()).trim(),
            flag: (await meeting.locator('img').nth(0).getAttribute('data-src') ?? '').split('.transform').at(0),
            title: (await meeting.locator('.event-title').innerText()).trim(),
            startDate: (await meeting.locator('.start-date').innerText()).trim(),
            endDate: (await meeting.locator('.end-date').innerText()).trim(),
            month: (await meeting.locator('.month-wrapper').innerText()).trim(),
            img: (await meeting.locator('img').nth(1).getAttribute('data-src') ?? '').split('.transform').at(0)?.replace("%20carbon", "")
        }
    }))
  })

//   https://media.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Bahrain%20carbon.png.transform/2col/image.png
//   https://media.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Saudi%20Arabia%20carbon.png.transform/2col/image.png