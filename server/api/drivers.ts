import { chromium } from 'playwright';

export default defineEventHandler(async(event) => {

    const browser = await chromium.launch();
    const context = await browser.newContext();
    const page = await context.newPage();

    await page.goto("https://www.formula1.com/en/drivers.html");

    const drivers = await page.locator('.listing-item--border').all();

    return Promise.all(drivers.map(async driver => {
        const name = await driver.locator('.listing-item--name')
        const firstName = (await name.locator('.f1--xxs').innerText())
        const lastName = (await name.locator('.f1-bold--s').innerText())
        const team = (await driver.locator('.listing-item--team').innerText())
        const pic = (await driver.locator('.listing-item--photo img').getAttribute('data-src')  ?? '').split('.transform').at(0)

        return {
            firstName: firstName,
            lastName: lastName,
            team: team,
            picture: pic
        }
    }))
  })