// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  routeRules: {
    '/': { ssr: false },
    '/login': { ssr: false },
  },
  modules: ['@nuxt/ui', "@nuxt/image"],
  css: ['~/assets/css/main.css'],
  tailwindcss: {
    config:{
      theme: {
        extend: {
          fontFamily: {
            'Titillium': ['Titillium', 'sans-serif'],
            'F1': ['F1', 'sans-serif'],
            'F1Wide': ['F1Wide', 'sans-serif'],
          },
        },
      }
    }
  }
})