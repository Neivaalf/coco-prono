import { Client, Account, Databases} from 'appwrite';

export const client = new Client();

client
    .setEndpoint('https://cloud.appwrite.io/v1')
    .setProject('coco-prono'); // Replace with your project ID

export const account = new Account(client);
export { ID, Query } from 'appwrite';

export const databases = new Databases(client);

export const DATABASE_ID = '65da0bd9896b1f6f1b91'
export const COLLECTION_ID = '65da4cf48ba7ad03f605'
export const API = 'https://ergast.com/api/f1/'
export const YEAR = '2023'

/*
Grille de départ : Pronostic des 3 premiers
 - Pilote correctement placé = 15 points (+5 points pour la pole position)
 - Pilote placé à +/- 1 place d'écart = 10 points
 - Pilote placé à +/- 2 places d'écart = 5 points
 - Pilote placé à +/- 3 places d'écart = 1 point

Course : Pronostic des 3 premiers
 - Pilote correctement placé = 25 points (+5 points pour la victoire)
 - Pilote placé à +/- 1 place d'écart = 18 points
 - Pilote placé à +/- 2 places d'écart = 12 points
 - Pilote placé à +/- 3 places d'écart = 8 points
 - Pilote placé à +/- 4 places d'écart = 4 points
 - Pilote placé à +/- 5 places d'écart = 2 points
 - Pilote placé à +/- 6 places d'écart = 1 point

Bonus
 - Meilleur tour en course = 10 points
 - Bonus Podium dans l'ordre = 20 points
 - Bonus Hat Trick (Pole + Victoire + Meilleur Tour) = 20 points
*/
export const racePoints = [25, 18, 12, 10, 8, 6, 4, 2, 1]
export const qualiPoints = [15, 10, 5, 1]